<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              wisdmlabs.com
 * @since             1.0.0
 * @package           Wdm_Wp_Book
 *
 * @wordpress-plugin
 * Plugin Name:       WP-Book
 * Plugin URI:        wisdmlabs.com
 * Description:       Wp Book Assignment.
 * Version:           1.0.0
 * Author:            WisdmLabs
 * Author URI:        wisdmlabs.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wdm-wp-book
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

//definations
define( 'WDM_WP_BOOK_VERSION', '1.0.0' );
$mpb_prefix = 'wpb_';
$mpb_plugin_name = 'Wp Book';
$wpb_settings = get_option('wpb_settings');

//loader
load_plugin_textdomain('wp-book', false, dirname( plugin_basename(__FILE__) ) . '/languages/' );

//hooks
add_action( 'init', 'wpb_book_register_custom_table' );
add_action( 'switch_blog', 'wpb_book_register_custom_table' );

//Registerations
register_activation_hook( __FILE__, 'wpb_custom_table' );

//requires
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_custom_book_post.php'); 
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_custom_book_category_hierarchical_taxonomy.php'); 
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_custom_book_category_non_hierarchical_taxonomy.php'); 
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_custom_metabox.php'); 
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_settings_page.php');
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_book_shortcode.php'); 
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_bookcategory_widget.php');
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_book_dashboard_widget.php');
include(plugin_dir_path( dirname( __FILE__ ) ).'wp-book/includes/wdm_selected_book_category_display_widget.php');

//Custom Table function
function wpb_custom_table(){ 

    global $wpdb;
    $table_name = $wpdb->prefix.'bookmeta';
    if( $wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name){
        $sql = "CREATE TABLE  $table_name (
            meta_id INTEGER (10) UNSIGNED AUTO_INCREMENT,
            book_id bigint(20) NOT NULL DEFAULT '0',
            meta_key varchar(255) DEFAULT NULL,
            meta_value longtext,
            PRIMARY KEY  (meta_id),
            KEY book_id (book_id),
            KEY meta_key(meta_key)
        )";

    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    }
}

function wpb_book_register_custom_table() {
    global $wpdb;
    $wpdb->bookmeta = $wpdb->prefix . 'bookmeta';
    $wpdb->tables[] = 'bookmeta';
    return;
}