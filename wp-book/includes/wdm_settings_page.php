<?php

function wdm_settings_page() {

    global $wdm_settings;

	ob_start(); ?>
	<div class="wrap">
		<h2>My First WordPress Plugin Options</h2>
		<form method="post" action="options.php">
            <?php settings_fields('wdm_settings_group'); ?>
            
            <h4>Currency Options</h4>

            <p>
                <?php $diff_currency = array('USD', 'INR', 'CAD', 'EUR'); ?>
                <select id="wdm_settings[currency]" name="wdm_settings[currency]">
                    <?php foreach($diff_currency as $currecy){ ?>
                        <?php if($wdm_settings['currency'] == $currecy) { $selected = 'selected="selected"'; } else{ $selected=''; } ?>
                        <option value="<?php echo $currecy; ?>"<?php echo $selected; ?>> <?php echo $currecy;?></option>
                    <?php } ?>
                </select>
                <label class="description" for="wdm_settings[number_of_books]"><?php _e('Select Currency','wp-book'); ?></label>
            </p>

            <h4>Number of Books displayed per page</h4>
            
            <p>
                <input id="wdm_settings[number_of_books]" name="wdm_settings[number_of_books]" type="number" value="<?php echo $wdm_settings['number_of_books']; ?>" />
                <label class="description" for="wdm_settings[number_of_books]"><?php _e('Enter Number of Books per page','wp-book'); ?></label>
            </p>

            <p>
                <input type="submit" class="button-primary" value="Save Options" />
            </p>

        </form>
	</div>
	<?php
	echo ob_get_clean();
}

function wdm_add_options_link() {
	add_menu_page(
        __('Book Admin Settings Page', 'wp-book'),
        __('Book Admin Settings', 'wp-book'),
        'manage_options',
        'wdm-settings',
        'wdm_settings_page',
        '',
        35
    );
}
add_action('admin_menu', 'wdm_add_options_link');

function wdm_register_settings(){
    register_setting('wdm_settings_group', 'wdm_settings');
}
add_action('admin_init', 'wdm_register_settings');