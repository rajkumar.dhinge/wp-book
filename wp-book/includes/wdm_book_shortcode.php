<?php 
/**
 * Shortcode for cpt book
 */
    function wdm_book_shortcode( $atts ){
        $atts = shortcode_atts(
            array(
                'book_id' => '',
                'author_name' => '',
                'year' => '',
                'category' => '',
                'tag' => '',
                'publisher' => ''
            ),
            $atts
        );
        
        $args = array(
                'post_type' => 'book',
                'post_status' => 'publish',
                'author' => $atts['author_name'],
            );
            
            if($atts['book_id'] != ''){
                $args['book_id'] = $atts['book_id'];
            }
            if($atts['category'] != ''){
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'book_category',
                        'terms' => array( $atts['category'] ),
                        'field' => 'name',
                        'operator' => 'IN'
                    ),
                );
            }
            if($atts['tag'] != ''){
                $args[ 'tax_query' ] = array(
                    array(
                        'taxonomy' => 'book_tags',
                        'terms' => array($atts['tag']),
                        'field' => 'name',
                        'operator' => 'IN'
                    ),
                );
            }
        return wdm_book_shortcode_function( $args );
    }
    
    //adding the shortcode
    add_shortcode('book','wdm_book_shortcode'); 
    
    
    // function for rendering the data
    function wdm_book_shortcode_function( $args ){
        
        global $wdm_settings;
        
        $wdm_query = new WP_Query( $args );
        if ( $wdm_query->have_posts() ) {
            while($wdm_query->have_posts()){
                $wdm_query->the_post();
                
                //retriving the meta info of book from database
                $wdm_info_author_name = get_metadata( 'book', get_the_id(), 'author-name' )[0];
                $wdm_info_price = get_metadata( 'book', get_the_id(), 'price' )[0];
                $wdm_info_publisher = get_metadata( 'book', get_the_id(), 'publisher' )[0];
                $wdm_info_year = get_metadata( 'book', get_the_id(), 'year' )[0];
                $wdm_info_edition = get_metadata( 'book', get_the_id(), 'edition' )[0];
                $wdm_info_url = get_metadata( 'book', get_the_id(), 'url' )[0];
                
                ?>
                <ul>
                <?php
                    if ( get_the_title() != '' ){
                        ?>
                        <li>Title: <a href="<?php get_post_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                        <?php
                    }
                    if( $wdm_info_author_name != ''  ){
                        ?>
                        <li>Author: <?php echo $wdm_info_author_name; ?></li>
                        <?php
                    }
                    if( $wdm_info_price != '' ){
                        ?>
                        <li>Price: <?php echo $wdm_info_price . ' ' . $wdm_settings[ 'currency' ] ; ?></li>
                        <?php
                    }
                    if( $wdm_info_publisher != '' ){
                        ?>
                        <li>Publisher: <?php echo $wdm_info_publisher; ?></li>
                        <?php
                    }
                    if( $wdm_info_year != '' ){
                        ?>
                        <li>Year: <?php echo $wdm_info_year; ?></li>
                        <?php
                    }
                    if( $wdm_info_edition != '' ){
                        ?>
                        <li>Edition: <?php echo $wdm_info_edition; ?></li>
                        <?php
                    }
                    if( $wdm_info_url  != '' ){
                        ?>
                        <li>Url: <?php echo $wdm_info_url; ?></li>
                        <?php
                    }
                    if ( get_the_content() != '' ) {
                        ?>
                        <li>Content: <?php echo get_the_content(); ?></li>
                        <?php
                    }
                ?>
                </ul>
                <?php
            }
        }else {
            ?>
            <h1>Sorry no Books Found</h1>
            <?php
        }
    }